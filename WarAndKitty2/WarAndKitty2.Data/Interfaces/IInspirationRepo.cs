﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Interfaces
{
     public interface IInspirationRepo
    {
        void AddInspiration(Inspiration inspiration);

        List<Inspiration> GetAll();

        void RemoveInspiration(int Id);

        Inspiration GetById(int Id);

        void EditInspiration(Inspiration inspiration);
    }
}
