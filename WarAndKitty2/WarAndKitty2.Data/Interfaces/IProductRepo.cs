﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Interfaces
{
   public interface IProductRepo
    {
        void AddProduct(Product product);

        List<Product> GetAll();

        void RemoveProduct(int Id);

        Product GetById(int Id);

        void EditProduct(Product product);
    }
}
