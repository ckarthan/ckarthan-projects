﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Interfaces
{
   public interface IRegisterRepo
    {
        void AddRegister(Register register);

        List<Register> GetAll();

        void RemoveRegister(int Id);

        Register GetById(int Id);

        void EditRegister(Register register);
    }
}
