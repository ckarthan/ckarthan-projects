﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Data.Repositories;

namespace WarAndKitty2.Data.Factories
{
   public class RegisterFactory
    {
        public static IRegisterRepo CreateRegisterRepo()
        {
            IRegisterRepo repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();

            switch (mode)
            {
                case "TEST":
                    repo = new MockRegister();
                    break;
                //case "PROD":
                //    repo = new DBRegisterRepo();
                //    break;
                default:
                    throw new NotImplementedException();
            }

            return repo;
        }
    }
}
