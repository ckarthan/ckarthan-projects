﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Data.Repositories;

namespace WarAndKitty2.Data.Factories
{
    public class InspirationFactory
    {

        public static IInspirationRepo CreateInspirationRepo()
        {
            IInspirationRepo repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToUpper();

            switch (mode)
            {
                case "TEST":
                    repo = new MockInspiration();
                    break;
                //case "PROD":
                //    repo = new DBinspirationRepo();
                //    break;
                default:
                    throw new NotImplementedException();
            }

            return repo;
        }
    }
}


