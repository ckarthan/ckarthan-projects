﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Data.Repositories;

namespace WarAndKitty2.Data.Factories
{
   public class ProductFactory
    {
        public static IProductRepo CreateProductRepo()
        {
            IProductRepo repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();

            switch (mode)
            {
                case "TEST":
                    repo = new MockProduct();
                    break;
                //case "PROD":
                //    repo = new DBProductRepo();
                //    break;
                default:
                    throw new NotImplementedException();
            }

            return repo;
        }
    }
}
