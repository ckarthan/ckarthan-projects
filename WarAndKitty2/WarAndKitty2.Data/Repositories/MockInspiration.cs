﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Repositories
{
    public class MockInspiration : IInspirationRepo
    {
        private static List<Inspiration> _inspirations;

        static MockInspiration()
        {
            _inspirations = new List<Inspiration>
            {
                new Inspiration
                {
                    InspirationId = 1,
                    InspirationImage = "/Content/Images/Interior Paint Colors.jpg",
                    InspirationLink = "http://www.elledecor.com/design-decorate/color/g3175/color-trends/",
                    InspirationDescription = "The Hottest Interior Colors of 2017"
                },
                new Inspiration
                {
                    InspirationId = 2,
                    InspirationImage = "/Content/Images/Painted Floors.PNG",
                    InspirationLink = "http://www.houzz.com/photos/traditional/query/painted-wood-floor/p/8",
                    InspirationDescription ="Painted Floors for a Grand Transformation"
                },
                new Inspiration
                {
                    InspirationId = 3,
                    InspirationImage = "/Content/Images/Fireplace Makeover.jpg",
                    InspirationLink = "https://www.lowes.com/creative-ideas/other-areas/modern-fireplace-makeover/project",
                    InspirationDescription = "DIY Project: Transform that Old Fireplace"
                }
            };
        }

        public void AddInspiration(Inspiration inspiration)
        {
            _inspirations.Add(inspiration);
        }

        public void EditInspiration(Inspiration inspiration)
        {
            var editInspiration = _inspirations.Find(i => i.InspirationId == inspiration.InspirationId);
            _inspirations.Remove(editInspiration);
            AddInspiration(inspiration);
        }

        public List<Inspiration> GetAll()
        {
            return _inspirations;
        }

        public Inspiration GetById(int Id)
        {
            var getInspiration = _inspirations.Find(i => i.InspirationId == Id);
            return getInspiration;
        }

        public void RemoveInspiration(int Id)
        {
            var oneInspiration = GetById(Id);
            _inspirations.Remove(oneInspiration);
        }
    }
}
