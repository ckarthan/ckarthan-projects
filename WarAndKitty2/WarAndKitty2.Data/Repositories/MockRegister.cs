﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Repositories
{
    public class MockRegister:IRegisterRepo
    {
        private static List<Register> _register;

        static MockRegister()
        {
            _register = new List<Register>
            {
                new Register
                {
                    RegistrationId = 1,
                    FirstName = "Nikki",
                    LastName = "Parthan",
                    Email = "nparthan@yahoo.com",
                    FavoriteMaterial = Material.Brass,
                    Comments = "I would like to see some brass elephants, please keep your eyes peeled!"
                },
                new Register
                {
                    RegistrationId = 2,
                    FirstName = "Vikki",
                    LastName = "Charthan",
                    Email = "vcharthan@yahoo.com",
                    FavoriteMaterial = Material.Silver,
                    Comments = "I hate crystal, it's just not my thing."
                },
                  new Register
                {
                    RegistrationId = 3,
                    FirstName = "Mikki",
                    LastName = "Barthan",
                    Email = "mbarthan@gmail.com",
                    FavoriteMaterial = Material.Pewter,
                    Comments = "Pewter das guten!"
                }
            };          
           
        }

        public void AddRegister(Register register)
        {
            _register.Add(register);
        }

        public void EditRegister(Register register)
        {
            var editRegister = _register.Find(r => r.RegistrationId == register.RegistrationId);
            _register.Remove(editRegister);
            AddRegister(register);
        }

        public List<Register> GetAll()
        {
            return _register;
        }

        public Register GetById(int Id)
        {
            return _register.Find(r => r.RegistrationId == Id);
        }

        public void RemoveRegister(int Id)
        {
            var oneRegister = GetById(Id);
            _register.Remove(oneRegister);
        }
    }
}
