﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Models;

namespace WarAndKitty2.Data.Repositories
{
   public class MockProduct: IProductRepo
    {
       private static List<Product> _products;

        static MockProduct()
        {
            _products = new List<Product>
            {
                new Product
                {
                    ProductId = 1,
                    ProductTitle = "Waterford Lismore Bowl- 6 inch",
                    ProductDescription = "Stunning crystal bowl in the Lismore pattern by Waterford crystal. Features etched Waterford mark.",
                    ProductImage = "/Content/Images/LismoreBowl.PNG",
                    ProductLink = "https://www.etsy.com/listing/494402885/waterford-lismore-6-crystal-bowl?ref=shop_home_active_51",
                    ProductType = Material.Crystal
                },
                new Product
                {
                    ProductId = 2,
                    ProductTitle = "Set of 2 Brass Sconces by Sadek",
                    ProductDescription = "Set of two beautiful brass wall sconces. Brass feature on back for secure hanging. Sticker says: Andrea by Sadek.",
                    ProductImage = "/Content/Images/BrassSconces.PNG",
                    ProductLink = "https://www.etsy.com/listing/449019188/ornate-brass-wall-sconces?ref=shop_home_active_44",
                    ProductType = Material.Brass
                },
                new Product
                {
                    ProductId = 3,
                    ProductTitle = "Ceramic Berry Jar",
                    ProductDescription = "Whimsical off-white ceramic berry bowl with brass lid. This vintage covered bowl is dimpled, just like a berry, and has a brass dome lid that is topped off with an etched berry pull. ",
                    ProductImage = "/Content/Images/CeramicBerryJar.PNG",
                    ProductLink = "https://www.etsy.com/listing/246840171/ceramic-berry-jar-with-metal-leafed-lid?ref=shop_home_active_22",
                    ProductType = Material.Ceramic
                },
                new Product
                {
                    ProductId = 4,
                    ProductTitle = "Set of 5 Silverplate Coasters",
                    ProductDescription = "Set of five silver-plate on steel drink coasters. Lovely carved design on face and detailed rim. A charming set of vintage coasters.",
                    ProductImage = "/Content/Images/SilverCoasters.PNG",
                    ProductLink = "https://www.etsy.com/listing/480947662/silverplated-coasters-s5?ref=shop_home_active_52",
                    ProductType = Material.Silver
                },
                 new Product
                {
                    ProductId = 5,
                    ProductTitle = "Brass Ibex Bookend- Single",
                    ProductDescription = "Vintage solid brass Ibex bookend- one bookend only. Very striking, small areas of wear/ oxidation. Wonderful piece nice golden finish, heavy patina.",
                    ProductImage = "/Content/Images/Ibex.PNG",
                    ProductLink = "https://www.etsy.com/listing/494431333/vintage-brass-ibex-single-bookend?ref=shop_home_feat_1",
                    ProductType = Material.Brass
                },
                 new Product
                {
                    ProductId = 6,
                    ProductTitle = "Midcentury Drinkware Set",
                    ProductDescription = "Midcentury drink set with pitcher and two glasses. Matching bands around the lip of all 3 feature a repeating fleur-de-lis pattern. The band is silver on the outside and green on the inside, adding a little whimsy to this set.",
                    ProductImage = "/Content/Images/MidcenturyDrinkSet.PNG",
                    ProductLink = "https://www.etsy.com/listing/194978157/midcentury-drink-set?ref=shop_home_active_68",
                    ProductType = Material.Glass
                }
            };
        }

        public void AddProduct(Product product)
        {
            _products.Add(product);
        }

        public List<Product> GetAll()
        {
            return _products;
        }

        public void RemoveProduct(int Id)
        {
            var oneProduct = _products.Find(p => p.ProductId == Id);
            _products.Remove(oneProduct);
        }

        public Product GetById(int Id)
        {
            return _products.Find(p => p.ProductId == Id);
        }

        public void EditProduct(Product product)
        {
            var editProduct = GetById(product.ProductId);
            _products.Remove(editProduct);
            AddProduct(product);
        }
       
    }
}
