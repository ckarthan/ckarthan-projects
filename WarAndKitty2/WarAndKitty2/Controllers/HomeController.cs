﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WarAndKitty2.BLL;
using WarAndKitty2.Models;

namespace WarAndKitty2.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult About()
        {
            ViewBag.Message = "Let's Get Inspired...";
           InspirationManager manager = new InspirationManager();
            var inspiration = manager.GetInspirations();
            return View(inspiration);
        }
        [HttpGet]
        public ActionResult Products()
        {
            ViewBag.Message = "Featured Products by War & Kitty";
            ProductManager manager = new ProductManager();
            var products = manager.GetProducts();
            return View(products);
        }
        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Us";
            var contact = new Register();
            return View(contact);
        }

        [HttpPost]
        public ActionResult Contact(Register register)
        {
            RegisterManager manager = new RegisterManager();

            if (ModelState.IsValid)
            {
                manager.Add(register);
                return View("Confirm");
            }
            return View();
        }
    }
}