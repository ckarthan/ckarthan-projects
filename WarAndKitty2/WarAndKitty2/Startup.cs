﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WarAndKitty2.Startup))]
namespace WarAndKitty2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
