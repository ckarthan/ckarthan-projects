﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarAndKitty2.Models
{
    public class Inspiration
    {
        public int InspirationId { get; set; }
        public string InspirationImage { get; set; }
        public string InspirationLink { get; set; }
        public string InspirationDescription { get; set; }
    }
}
