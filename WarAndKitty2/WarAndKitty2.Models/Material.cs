﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarAndKitty2.Models
{
    public enum Material
    {
        Brass,
        Silver,
        Gold,
        Ceramic,
        Glass,
        Crystal,
        Pewter,
        Porcelain,
        Other
    }
}
