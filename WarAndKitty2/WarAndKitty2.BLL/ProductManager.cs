﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Data.Factories;
using WarAndKitty2.Models;

namespace WarAndKitty2.BLL
{
    public class ProductManager
    {
        private IProductRepo _productRepo;

        public ProductManager()
        {
            _productRepo = ProductFactory.CreateProductRepo();
        }

        public void Add(Product product)
        {
            _productRepo.AddProduct(product);
        }

        public void Remove(int prodId)
        {
            _productRepo.RemoveProduct(prodId);
        }

        public void Edit(Product product)
        {
            _productRepo.EditProduct(product);
        }

        public List<Product>GetProducts()
        {
            return _productRepo.GetAll();
        }

        public Product GetOneProduct(int prodId)
        {
            return _productRepo.GetById(prodId);
        }
    }
}
