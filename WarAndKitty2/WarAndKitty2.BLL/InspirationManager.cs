﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Factories;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Models;

namespace WarAndKitty2.BLL
{
    public class InspirationManager
    {
        private IInspirationRepo _inspiration;

        public InspirationManager()
        {
            _inspiration = InspirationFactory.CreateInspirationRepo();
        }

        public void Add(Inspiration inspiration)
        {
            _inspiration.AddInspiration(inspiration);
        }

        public void Remove(int inspireId)
        {
            _inspiration.RemoveInspiration(inspireId);
        }

        public void Edit(Inspiration inspiration)
        {
            _inspiration.EditInspiration(inspiration);
        }

        public List<Inspiration>GetInspirations()
        {
            return _inspiration.GetAll();
        }

        public Inspiration GetOneInspiration(int inspireId)
        {
            return _inspiration.GetById(inspireId);
        }
    }
}
