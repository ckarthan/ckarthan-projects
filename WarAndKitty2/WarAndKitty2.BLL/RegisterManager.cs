﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAndKitty2.Data.Factories;
using WarAndKitty2.Data.Interfaces;
using WarAndKitty2.Models;

namespace WarAndKitty2.BLL
{
    public class RegisterManager
    {
        private IRegisterRepo _register;

        public RegisterManager()
        {
            _register = RegisterFactory.CreateRegisterRepo();
        }

        public void Add(Register register)
        {
            _register.AddRegister(register);
        }

        public void Remove(int regId)
        {
            _register.RemoveRegister(regId);
        }

        public void Edit(Register register)
        {
            _register.EditRegister(register);
        }

        public List<Register>GetAllRegisters()
        {
            return _register.GetAll();
        }

        public Register GetOneRegister(int regId)
        {
            return _register.GetById(regId);
        }
    }
}
