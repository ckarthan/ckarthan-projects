﻿$(document)
    .ready(function () {
        $("#addOrder")
            .validate({
                rules: {
                    "Order.CustomerName": {
                        required: true
                    },
                    "Order.OneState.StateName": {
                        required: true     
                    },
                    "OrderOneState_StateName": {
                        required: true
                    },
                    "Order.Area": {
                        required: true,
                        greaterThan: 99.00
                    }
                },
                messages: {
                    CustomerName: {
                        required: "Please enter customer name"
                    },
                    OneProduct_ProductName: {
                        required: "Please select a product"  
                    },
                    OneState_StateName: {
                        required: "Please select a state"             
                    },
                    Area: {
                        required: "Please enter room area",
                        greaterThan: "Minimum area is 100 square feet"
                    }
                }

            });
    });





