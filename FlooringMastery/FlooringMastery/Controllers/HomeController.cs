﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlooringMastery.BLL;
using FlooringMastery.Models;

namespace FlooringMastery.Controllers
{
    public class HomeController : Controller
    {
        OrderManager manager = new OrderManager();
        ProductManager pmanager = new ProductManager();
        StateManager smanager = new StateManager();

        [HttpGet]
        public ActionResult FlooringStart()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Display(DateTime? orderDate)
        {
           
            DisplayOrdersViewModel vm = new DisplayOrdersViewModel();
            vm.OrderDate = orderDate;
            vm.Orders  = manager.DisplayOrders(orderDate.GetValueOrDefault());
           
            return View(vm);       
        }

       

        [HttpGet]
        public ActionResult Remove(int orderNumber)
        {
            Order orderToRemove = manager.GetOrderById(orderNumber);
            return View(orderToRemove);
        }

        [HttpPost]
        public ActionResult Remove(Order order)
        {
            manager.RemoveOrder(order.OrderNumber);
            return View("DeleteConfirmation");
        }

        [HttpGet]
        public ActionResult Edit(int orderNumber)
        {
            AddOrderViewModel vm = new AddOrderViewModel
            {
                Products = pmanager.GetAllProducts(),
                States = smanager.GetAllStates(),
                Order = manager.GetOrderById(orderNumber)
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AddOrderViewModel vm)
        {
            var product = pmanager.GetProductById(vm.Order.OneProduct.ProductId);
            vm.Order.OneProduct = product;

            var state = smanager.GetStateById(vm.Order.OneState.StateId);
            vm.Order.OneState = state;


            if (!ModelState.IsValid)
            {
                vm.Products = pmanager.GetAllProducts();
                vm.States = smanager.GetAllStates();
                return View(vm);


            }

            manager.EditOrder(vm.Order);
            return View("EditConfirmation");

        }

        [HttpGet]
        public ActionResult Add()
        {
            AddOrderViewModel vm = new AddOrderViewModel
            {
                Products = pmanager.GetAllProducts(),
                States = smanager.GetAllStates(),
                Order = new Order()
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddOrderViewModel vm)
        {
            var product = pmanager.GetProductById(vm.Order.OneProduct.ProductId);
            vm.Order.OneProduct = product;
            
            var state = smanager.GetStateById(vm.Order.OneState.StateId);
            vm.Order.OneState = state;

            if (!ModelState.IsValid)
            {
                vm.Products = pmanager.GetAllProducts();
                vm.States = smanager.GetAllStates();
                return View(vm);
                
            }

            manager.AddOrder(vm.Order);
            return View("AddConfirmation");

        }
    }
}