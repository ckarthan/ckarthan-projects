﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlooringMastery.Models
{
    public class DisplayOrdersViewModel
    {
        public DateTime? OrderDate { get; set; }
        public List<Order> Orders { get; set; }
    }
}