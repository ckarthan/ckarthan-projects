﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlooringMastery.Models
{
    public class AddOrderViewModel
    {
        public Order Order { get; set; }
        public List<Product> Products { get; set; }
        public List<State> States { get; set; }
    }
}