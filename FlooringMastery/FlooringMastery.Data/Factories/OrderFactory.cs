﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using FlooringMastery.Data.Repositories;

namespace FlooringMastery.Data.Factories
{
    public static class OrderFactory
    {
        public static IOrder CreateOrderRepo()
        {
           
            string mode = ConfigurationManager.AppSettings["mode"].ToUpper();

            switch (mode)
            {
                case "TEST":
                    return new MockOrder();
                case "PROD":
                    return new OrderDB();
                default:
                    throw new NotImplementedException();
            }
          
        }
         
    }
}
