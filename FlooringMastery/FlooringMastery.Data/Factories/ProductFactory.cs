﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Data.Repositories;

namespace FlooringMastery.Data.Factories
{
    public static class ProductFactory
    {
        public static IProduct CreateProductRepo()
        {

            string mode = ConfigurationManager.AppSettings["mode"].ToUpper();

            switch (mode)
            {
                case "TEST":
                    return new MockProduct();
                case "PROD":
                    return new ProductDB();
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
