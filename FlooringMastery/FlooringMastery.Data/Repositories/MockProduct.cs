﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class MockProduct: IProduct
    {
        private static List<Product> _products;

        static MockProduct()
        {
            _products = new List<Product>
            {
                new Product
                {
                    ProductId = 1,
                    ProductName = "CARPET",
                    CostSqFt = 2.25M,
                    LaborSqFt = 2.10M
                },
                new Product
                {
                    ProductId = 2,
                    ProductName = "LAMINATE",
                    CostSqFt = 1.75M,
                    LaborSqFt = 2.10M
                },
                new Product
                {
                    ProductId = 3,
                    ProductName = "TILE",
                    CostSqFt = 3.50M,
                    LaborSqFt = 4.15M
                },
                new Product
                {
                    ProductId = 4,
                    ProductName = "HARDWOOD",
                    CostSqFt = 5.15M,
                    LaborSqFt = 4.75M
                }
            };
        }

        public Product GetProductById(int id)
        {
           return _products.Find(p => p.ProductId == id);
        }

        public List<Product> GetAllProducts()
        {
            return _products;
        }
    }
}
