﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class ProductDB : IProduct
    {
        private string _DBConnectionString;

        public ProductDB()
        {
            _DBConnectionString = ConfigurationManager.ConnectionStrings["FlooringMastery"].ConnectionString;
        }

        public List<Product> GetAllProducts()
        {

            var products = new List<Product>();
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    CommandText = "SELECT * FROM Products",
                    Connection = connection
                };
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var product = CastToProduct(reader);

                        products.Add(product);
                    }
                }
            }
            return products;
        }

       
        private Product CastToProduct(SqlDataReader reader)
        {
            return new Product()
            {
                ProductId = (int) reader["ProductId"],
                ProductName = reader["ProductName"].ToString(),
                CostSqFt = (decimal) reader["CostSqFt"],
                LaborSqFt = (decimal) reader["LaborSqFt"]
            };
        }

        public Product GetProductById(int id)
        {
            List<Product> products = GetAllProducts();
            Product product = products.Find(p => p.ProductId == id);
            return product;
        }
    }
}
