﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class OrderDB : IOrder
    {

        private string _DBConnectionString;

        public OrderDB()
        {
            _DBConnectionString = ConfigurationManager.ConnectionStrings["FlooringMastery"].ConnectionString;
        }

        public List<Order> GetAllOrders()
        {
            var orders = new List<Order>();
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "GetAllOrders";
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var order = CastToOrder(reader);

                        orders.Add(order);
                    }
                }
            }
            return orders.OrderBy(o => o.OrderNumber).ToList();
        }

        private Order CastToOrder (SqlDataReader reader)
        {
            ProductDB prodDB = new ProductDB();
            StateDB stateDB = new StateDB();

            return new Order()
            {
                OrderNumber = (int)reader["OrderNumber"],
                OrderDate = (DateTime)reader["OrderDate"],
                CustomerName = reader["CustomerName"].ToString(),
                Area = (decimal)reader["Area"],
                OneProduct = prodDB.GetProductById((int) reader ["ProductId"]),
                OneState = stateDB.GetStateById((int) reader ["StateId"]),
                MaterialTotal = (decimal)reader["MaterialTotal"],
                LaborTotal = (decimal)reader["LaborTotal"],
                TotalTax = (decimal)reader["TotalTax"],
                Total = (decimal)reader["Total"]
            };
        }

        public void AddOrder(Order order)
        {
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {
  
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "AddOrder";
                command.Parameters.AddWithValue("@OrderDate", order.OrderDate);
                command.Parameters.AddWithValue("@CustomerName", order.CustomerName);
                command.Parameters.AddWithValue("@Area", order.Area);
                command.Parameters.AddWithValue("@ProductId", order.OneProduct.ProductId); 
                command.Parameters.AddWithValue("@StateId", order.OneState.StateId); 
                command.Parameters.AddWithValue("@MaterialTotal", order.MaterialTotal);
                command.Parameters.AddWithValue("@LaborTotal", order.LaborTotal);
                command.Parameters.AddWithValue("@TotalTax", order.TotalTax);
                command.Parameters.AddWithValue("@Total", order.Total);
                connection.Open();
                command.ExecuteNonQuery();

            }
        }

        public void RemoveOrder(int orderNumber)
        {
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "RemoveOrder";
                connection.Open();
                command.Parameters.AddWithValue("@OrderNumber", orderNumber);
                command.ExecuteNonQuery();

            }
        }

        public Order GetOrderById(int orderNumber)
        {
            List<Order> orders = GetAllOrders();
            Order order = orders.Find(o => o.OrderNumber == orderNumber);
            return order; 
        }

        public void EditOrder(Order order)
        {
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {

                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "EditOrder";
                connection.Open();
                command.Parameters.AddWithValue("@OrderNumber", order.OrderNumber);
                command.Parameters.AddWithValue("@CustomerName", order.CustomerName);
                command.Parameters.AddWithValue("@ProductId", order.OneProduct.ProductId);
                command.Parameters.AddWithValue("@StateId", order.OneState.StateId);
                command.Parameters.AddWithValue("@Area", order.Area);
                command.ExecuteNonQuery();
            }
        }
    }
}
