﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class StateDB: IState
    {
        private string _DBConnectionString;

        public StateDB()
        {
            _DBConnectionString = ConfigurationManager.ConnectionStrings["FlooringMastery"].ConnectionString;
        }

        public List<State> GetAllStates()
        {

            var states = new List<State>();
            using (SqlConnection connection = new SqlConnection(_DBConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    CommandText = "SELECT * FROM States",
                    Connection = connection
                };
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var state = CastToState(reader);

                        states.Add(state);
                    }
                }
            }
            return states;
        }


        private State CastToState(SqlDataReader reader)
        {
            return new State()
            {
                StateId = (int)reader["StateId"],
                StateName = reader["StateName"].ToString(),
                Abbreviation = reader["Abbreviation"].ToString(),
                TaxRate = (decimal)reader["TaxRate"]
            };
        }

        public State GetStateById(int id)
        {
            List<State> states = GetAllStates();
            State state = states.Find(s => s.StateId == id);
            return state;
        }
    }
}

