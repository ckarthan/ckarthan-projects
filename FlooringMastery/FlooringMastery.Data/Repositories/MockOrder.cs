﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class MockOrder:IOrder
    {
        private static List<Order>_orders;

        static MockOrder()
        {
            _orders = new List<Order>
            {
                new Order
                {
                    OrderDate = DateTime.Parse("02/01/2017"),
                    OrderNumber = 1,
                    CustomerName = "Frankies Flooring",
                    Area = 500,
                    OneState = new State
                    {
                        StateId = 1,
                        StateName = "OHIO",
                        Abbreviation = "OH",
                        TaxRate = 6.25M
                    },
                    OneProduct = new Product
                    {
                        ProductId = 4,
                        ProductName = "HARDWOOD",
                        CostSqFt = 5.15M,
                        LaborSqFt = 4.75M
                    }
                },
                new Order
                {
                    OrderDate = DateTime.Parse("02/21/2017"),
                    OrderNumber = 2,
                    CustomerName = "Tile Outlet. LLC",
                    Area = 1500,
                    OneState = new State
                    {
                        StateId = 2,
                        StateName = "PENNSYLVANIA",
                        Abbreviation = "PA",
                        TaxRate = 6.75M
                    },
                    OneProduct = new Product
                    {
                        ProductId = 3,
                        ProductName = "TILE",
                        CostSqFt = 3.50M,
                        LaborSqFt = 4.15M
                    }
                },
                new Order
                {
                    OrderDate = DateTime.Parse("02/21/2017"),
                    OrderNumber = 3,
                    CustomerName = "Floors, Floors, Floors",
                    Area = 550,
                    OneState = new State
                    {
                        StateId = 2,
                        StateName = "PENNSYLVANIA",
                        Abbreviation = "PA",
                        TaxRate = 6.75M
                    },
                    OneProduct = new Product
                    {
                        ProductId = 2,
                        ProductName = "LAMINATE",
                        CostSqFt = 1.75M,
                        LaborSqFt = 2.10M
                    }
                },
                new Order
                {
                    OrderDate = DateTime.Parse("02/21/2017"),
                    OrderNumber = 4,
                    CustomerName = "Fred's Carpeting",
                    Area = 800,
                    OneState = new State
                    {
                        StateId = 4,
                        StateName = "INDIANA",
                        Abbreviation = "IN",
                        TaxRate = 6.0M
                    },
                    OneProduct = new Product
                    {
                        ProductId = 1,
                        ProductName = "CARPET",
                        CostSqFt = 2.25M,
                        LaborSqFt = 2.10M
                    }
                }
            };
        }

        public List<Order> GetAllOrders()
        {
         return _orders.OrderBy(o => o.OrderNumber).ToList();
        }

        public void AddOrder(Order order)
        {
            if (order.OrderNumber == 0)
            {
                order.OrderNumber = _orders.Count + 1;
            }
            _orders.Add(order);
        }

        public void RemoveOrder(int orderId)
        {
            Order removeOrder = _orders.Find(o => o.OrderNumber == orderId);
            _orders.Remove(removeOrder);
        }

        public Order GetOrderById(int orderId)
        {
            Order oneOrder = _orders.Find(o => o.OrderNumber == orderId);
            return oneOrder;
        }

        public void EditOrder(Order order)
        {
            Order orderToEdit = _orders.Find(o => o.OrderNumber == order.OrderNumber);
            _orders.Remove(orderToEdit);
            _orders.Add(order);
        }

        
    }
}
