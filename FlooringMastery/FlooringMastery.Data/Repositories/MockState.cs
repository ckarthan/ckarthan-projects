﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data.Repositories
{
    public class MockState:IState
    {
        private static List<State>_states;
       
        static MockState()
        {
            _states = new List<State>
            {
                new State
                {
                    StateId = 1,
                    StateName = "OHIO",
                    Abbreviation = "OH",
                    TaxRate = 6.25M
                },
                new State
                {
                    StateId = 2,
                    StateName = "PENNSYLVANIA",
                    Abbreviation = "PA",
                    TaxRate = 6.75M
                },
                new State
                {
                    StateId = 3,
                    StateName = "MICHIGAN",
                    Abbreviation = "MI",
                    TaxRate = 5.75M
                },
                new State
                {
                    StateId = 4,
                    StateName = "INDIANA",
                    Abbreviation = "IN",
                    TaxRate = 6.00M
                }
            };

        }
               
        public List<State> GetAllStates()
        {
            return _states;
        }

        public State GetStateById(int id)
        {
            return _states.Find(s => s.StateId == id);
        }
    }
}
