﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data
{
    public interface IProduct
    {
        List<Product> GetAllProducts();

        Product GetProductById(int id);
    }
}
