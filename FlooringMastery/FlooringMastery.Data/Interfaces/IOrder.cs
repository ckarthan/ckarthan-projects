﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Models;

namespace FlooringMastery.Data
{
    public interface IOrder
    {
            List<Order> GetAllOrders();

            void AddOrder(Order order);

            void RemoveOrder(int orderId);

            Order GetOrderById(int orderId);

            void EditOrder(Order order);
        
    }
}
