﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Data.Repositories;
using FlooringMastery.Models;
using NUnit.Framework;

namespace FlooringMastery.Tests
{
    [TestFixture]
    public class DataTests
    {

        [Test]
        public void CanGetOrders()
        {
            MockOrder orderRepo = new MockOrder();
            var allOrders = orderRepo.GetAllOrders();
            Assert.AreEqual(allOrders.Count, 4);
        }

        [Test] 
        public void CanGetProducts()
        {
            MockProduct prodRepo = new MockProduct();
            var allProducts = prodRepo.GetAllProducts();
            Assert.AreEqual(allProducts.Count, 4);
        }

        [Test]
        public void CanGetOneProduct()
        {   
            MockProduct prodRepo = new MockProduct();
            var oneProduct = prodRepo.GetProductById(2);
            Assert.AreEqual(oneProduct.ProductId, 2 );
        }

        [Test]
        public void CanGetStates()
        {
            MockState stateRepo = new MockState();
            var allStates = stateRepo.GetAllStates();
            Assert.AreNotEqual(allStates.Count, 3);
        }

        [Test]
        public void CanGetOneState()
        {
            MockState stateRepo = new MockState();
            var oneState = stateRepo.GetStateById(3);
            Assert.AreEqual(oneState.Abbreviation, "IN");
        }

        [Test]
        public void CanGetOneOrder()
        {
            MockOrder orderRepo = new MockOrder();
            var oneOrder = orderRepo.GetOrderById(2);
            Assert.AreEqual(oneOrder.Area, 1500);
        } 

        [TestCase]
        public void CanEditOrder()
        {
            MockOrder orderRepo = new MockOrder();
            
            var editedOrder =
                new Order
                {
                    OrderDate = DateTime.Parse("2/20/2017"),
                    OrderNumber = 2,
                    CustomerName = "Crazy Ern's Bargain Madness",
                    Area = 150M,
                    OneProduct =
                        new Product
                        {
                            ProductId = 1,
                            ProductName = "CARPET",
                            LaborSqFt = .50M,
                            CostSqFt = .25M
                        },
                    OneState =
                        new State
                        {
                            Abbreviation = "AK",
                            StateId = 5,
                            StateName = "ALASKA",
                            TaxRate = .01M
                        }

                };
            orderRepo.EditOrder(editedOrder);
            var oneOrder = orderRepo.GetOrderById(2);
            Assert.AreEqual(oneOrder.Area, 150);
            Assert.AreEqual(oneOrder.OneState.StateName, "ALASKA");
        }
       
        [Test]
        public void CanAddOrder()
        {
            MockOrder orderRepo = new MockOrder();
            var newOrder =
            new Order
            {
                OrderDate = DateTime.Parse("02/20/2017"),
                OrderNumber = 5,
                CustomerName = "Crazy Ern's Bargain Madness",
                Area = 250M,
                OneProduct =
                        new Product
                        {
                            ProductId = 2,
                            ProductName = "TILE",
                            LaborSqFt = .50M,
                            CostSqFt = .25M
                        },
                OneState =
                        new State
                        {
                            Abbreviation = "AK",
                            StateId = 5,
                            StateName = "ALASKA",
                            TaxRate = .01M
                        }

            };
            orderRepo.AddOrder(newOrder);
            Assert.AreEqual(orderRepo.GetAllOrders().Count, 5);
            Assert.AreEqual(orderRepo.GetOrderById(5).OneProduct.ProductName, "TILE");
        }
        [Test]
        public void CanRemoveOrder()
        {
            MockOrder orderRepo = new MockOrder();
            orderRepo.RemoveOrder(1);
            Assert.AreEqual(orderRepo.GetAllOrders().Count, 3);
        }
        
    }
}
