﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Data;
using FlooringMastery.Data.Factories;
using FlooringMastery.Models;

namespace FlooringMastery.BLL
{
   public class ProductManager
    {
        private IProduct _productRepo = ProductFactory.CreateProductRepo();

        public List<Product> GetAllProducts()
        {
            return _productRepo.GetAllProducts();
        }

        public Product GetProductById(int id)
        {
            return _productRepo.GetProductById(id);
        }
    }
}
