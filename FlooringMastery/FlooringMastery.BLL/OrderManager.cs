﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Data;
using FlooringMastery.Data.Factories;
using FlooringMastery.Models;


namespace FlooringMastery.BLL
{
   public class OrderManager
    {
        private IOrder _orderRepo = OrderFactory.CreateOrderRepo();

        public List<Order> DisplayOrders(DateTime? dateEntered)
        {  
            var allOrders = _orderRepo.GetAllOrders();
                     
                foreach (var order in allOrders)
                {
                    CalculateTotals(order);
                }
            List<Order> ordersByDate = new List<Order>();
            if (dateEntered.HasValue)
            {
                ordersByDate = allOrders.Where(o => o.OrderDate == dateEntered).ToList();

            }

            return ordersByDate;
        }

        public Order GetOrderById(int id)
        {
            return _orderRepo.GetOrderById(id);
        }

        public void RemoveOrder(int id)
        {
            _orderRepo.RemoveOrder(id);
        }

        public void AddOrder(Order newOrder)
        {
            if (newOrder.Area < 100)
            {
                Exception ex = new Exception("Area must be at least 100 sq. feet!");
                throw ex;
            }
           
                newOrder.OrderDate = DateTime.Today;
                CalculateTotals(newOrder);
                _orderRepo.AddOrder(newOrder);

        }

        public void EditOrder(Order order)
        {
            if (order.Area >= 100)
            {
                CalculateTotals(order);
                _orderRepo.EditOrder(order);
            }
           
        }

        public Order CalculateTotals(Order newOrder)
        {
           
                newOrder.OneState.TaxRate = newOrder.OneState.TaxRate;
                newOrder.MaterialTotal = newOrder.OneProduct.CostSqFt * newOrder.Area;
                newOrder.LaborTotal = newOrder.OneProduct.LaborSqFt * newOrder.Area;
                newOrder.TotalTax = newOrder.OneState.TaxRate * (newOrder.MaterialTotal + newOrder.LaborTotal) / 100;
                newOrder.Total = newOrder.MaterialTotal + newOrder.LaborTotal + newOrder.TotalTax;
                return newOrder;
           
        }
    }
}
