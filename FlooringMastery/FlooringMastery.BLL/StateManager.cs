﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringMastery.Data;
using FlooringMastery.Data.Factories;
using FlooringMastery.Models;

namespace FlooringMastery.BLL
{
   public class StateManager
   {
       private IState _stateRepo = StateFactory.CreateStateRepo();

        public List<State> GetAllStates()
        {
            return _stateRepo.GetAllStates();
        }

       public State GetStateById(int id)
       {
           return _stateRepo.GetStateById(id);
       }
    }
}
