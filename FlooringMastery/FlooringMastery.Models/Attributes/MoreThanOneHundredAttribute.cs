﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringMastery.Models.Attributes
{
    public class MoreThanOneHundredAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is decimal)
            {
                if ((decimal)value > 99)
                    return true;
                else
                {
                    ErrorMessage = "You must enter an area of 100 or greater.";
                    return false;
                }
            }
            return false;
        }
    }
}
