﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringMastery.Models
{
    public class State
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string Abbreviation { get; set; }
        public decimal TaxRate { get; set; }
    }
}
