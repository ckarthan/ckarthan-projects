﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using FlooringMastery.Models.Attributes;

namespace FlooringMastery.Models
{
   public class Order 
    {
        
        public DateTime OrderDate { get; set; }
        public int OrderNumber { get; set;}
        [Required(ErrorMessage = "Please enter a customer name")]
        public string CustomerName { get;set; }
        [Required(ErrorMessage = "Please enter the room area")]
        [MoreThanOneHundred]
        public decimal Area { get; set; }
        [Required(ErrorMessage = "Please select your state")]
        public State OneState { get; set; }
        [Required(ErrorMessage = "Please select a product")]
        public Product OneProduct { get; set; }
        public decimal MaterialTotal { get; set; }
        public decimal LaborTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }

        public Order()
        {
            this.OneProduct = new Product();
            this.OneState = new State();
        }

       
    }
}
