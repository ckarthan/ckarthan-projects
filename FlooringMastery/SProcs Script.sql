USE SGFlooring
GO

IF EXISTS(
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_NAME = 'GetAllOrders')
BEGIN
DROP PROCEDURE GetAllOrders
END
GO

IF EXISTS(
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_NAME = 'AddOrder')
BEGIN
DROP PROCEDURE AddOrder
END
GO

IF EXISTS(
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_NAME = 'RemoveOrder')
BEGIN
DROP PROCEDURE RemoveOrder
END
GO

IF EXISTS(
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_NAME = 'EditOrder')
BEGIN
DROP PROCEDURE EditOrder
END
GO

CREATE PROCEDURE GetAllOrders
AS
SELECT *
FROM Orders
GO

CREATE PROCEDURE AddOrder(
@OrderDate datetime2, @CustomerName nvarchar, @Area decimal, @ProductId int, @StateId int, @MaterialTotal decimal, @LaborTotal decimal, @TotalTax decimal, @Total decimal
)
AS 
INSERT INTO Orders(OrderDate, CustomerName, Area, ProductId, StateId, MaterialTotal, LaborTotal, TotalTax, Total)
VALUES(@OrderDate, @CustomerName, @Area, @ProductId, @StateId, @MaterialTotal, @LaborTotal, @TotalTax, @Total)
GO

CREATE PROCEDURE RemoveOrder (@OrderNumber int)
AS 
DELETE Orders
WHERE @OrderNumber = orderNumber
GO

CREATE PROCEDURE EditOrder(@OrderNumber int, @CustomerName nvarchar, @ProductId int, @StateId int, @Area decimal)
AS
UPDATE Orders
SET CustomerName = @CustomerName, ProductId = @ProductId, StateId = @StateId, Area = @Area
WHERE orderNumber = @OrderNumber
GO