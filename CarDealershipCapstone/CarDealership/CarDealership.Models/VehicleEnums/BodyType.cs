﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models.VehicleEnums
{
   public enum BodyType
    {
     Car,
     Truck,
     SUV,
     Van,
     Hatchback,
     StationWagon,
     Convertible,
     Other
    }
}
