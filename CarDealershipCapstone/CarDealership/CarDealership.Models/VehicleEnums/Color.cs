﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models.VehicleEnums
{
    public enum Color
    {
        Black,
        Blue,
        Brown,
        Gold,
        Gray,
        Green,
        Orange,
        Pink,
        Purple,
        Red,
        Silver,
        Tan,
        White,
        Yellow,
        Other
    }
}
