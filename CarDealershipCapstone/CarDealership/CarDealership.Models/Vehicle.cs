﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models.VehicleEnums;

namespace CarDealership.Models
{
    public class Vehicle
    {
        public string VIN { get; set; }
        public MakeModel Make { get; set; }
        public int Year { get; set; }
        public VehicleType VehicleType { get; set; }
        public BodyType BodyType { get; set; }
        public Transmission Transmission { get; set; }
        public Color ExteriorColor { get; set; }
        public Color InteriorColor { get; set; }
        public int Mileage { get; set; }
        public decimal MSRP { get; set; }
        public decimal SalePrice { get; set; }
        public string Description { get; set; }
        public List<Image> Images { get; set; }
        public bool IsFeatured { get; set; }
    }
}
