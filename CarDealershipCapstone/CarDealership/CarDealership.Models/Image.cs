﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models
{
    public class Image
    {
        public int ImageId { get; set; }
        public string ImageLink { get; set; }
        public bool IsPrimaryImage { get; set; }
    }
}
