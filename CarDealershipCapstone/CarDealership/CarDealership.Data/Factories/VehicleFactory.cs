﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interfaces;
using CarDealership.Data.Repos;

namespace CarDealership.Data.Factories
{
    public static class VehicleFactory
    {
        public static IVehicle CreateVehicleRepo()
        {

            string mode = ConfigurationManager.AppSettings["mode"].ToUpper();

            switch (mode)
            {
                case "TEST":
                    return new VehicleMockRepo();
                case "PROD":
                //return new VehicleDB();
                default:
                    throw new NotImplementedException();
            }

        }

    }
}
