﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;

namespace CarDealership.Data.Interfaces
{
    public interface IVehicle
    {
        List<Vehicle> GetAllVehicles();
        Vehicle GetVehicleById(string vehicleId);
        void AddVehicle(Vehicle vehicle);
        void DeleteVehicle(string vehicleId);
        void EditVehicle(Vehicle vehicle);
    }
}
