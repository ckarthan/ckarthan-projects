﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interfaces;
using CarDealership.Models;
using CarDealership.Models.VehicleEnums;

namespace CarDealership.Data.Repos
{
    public class VehicleMockRepo:IVehicle
    {
        private static List<Vehicle> _vehicles;

        static VehicleMockRepo()
        {
            _vehicles = new List<Vehicle>
           {
               new Vehicle
               {
                   VIN = "12345ML8910",
                   Make = new MakeModel
                   {
                       MakeId = 1,
                       MakeName = "Toyota",
                       ModelName = "Camry"
                   },
                   Year = 2015,
                   VehicleType = VehicleType.Used,
                   BodyType = BodyType.Car,
                   Transmission = Transmission.Automatic,
                   ExteriorColor = Color.Black,
                   InteriorColor = Color.Black,
                   Mileage = 25590,
                   MSRP = 20500,
                   SalePrice = 17500,
                   Description = "Very clean Camry, only 2 years old. One owner, non-smoker. This car is a must see!",
                   Images = new List<Image>
                   {
                       new Image
                       {
                         ImageId = 1,
                         ImageLink = "/Content/CarImages/Camry1.JPG",
                         IsPrimaryImage = true
                       },
                       new Image
                       {
                           ImageId = 2,
                           ImageLink ="/Content/CarImages/Camry2.JPG",
                           IsPrimaryImage = false
                       },
                       new Image
                       {
                           ImageId = 3,
                           ImageLink = "/Content/CarImages/Camry3.JPG",
                           IsPrimaryImage = false
                       }
                   },
                   IsFeatured = true
               },
                new Vehicle
               {
                   VIN = "54345BP8HI0",
                   Make = new MakeModel
                   {
                       MakeId = 2,
                       MakeName = "Volkswagen",
                       ModelName = "CC"
                   },
                   Year = 2013,
                   VehicleType = VehicleType.Used,
                   BodyType = BodyType.Car,
                   Transmission = Transmission.Manual,
                   ExteriorColor = Color.White,
                   InteriorColor = Color.Tan,
                   Mileage = 31530,
                   MSRP = 21590,
                   SalePrice = 18950,
                   Description = "6 speed manual, fun sporty car. Clean car-fax, low miles.",
                   Images = new List<Image>
                   {
                       new Image
                       {
                         ImageId = 4,
                         ImageLink = "/Content/CarImages/VW1.JPG",
                         IsPrimaryImage = true
                       },
                       new Image
                       {
                           ImageId = 5,
                           ImageLink ="/Content/CarImages/VW2.JPG",
                           IsPrimaryImage = false
                       },
                       new Image
                       {
                           ImageId = 6,
                           ImageLink = "/Content/CarImages/VW3.JPG",
                           IsPrimaryImage = false
                       },
                        new Image
                       {
                           ImageId = 7,
                           ImageLink ="/Content/CarImages/VW4.JPG",
                           IsPrimaryImage = false
                       },
                         new Image
                       {
                           ImageId = 8,
                           ImageLink ="/Content/CarImages/VW5.JPG",
                           IsPrimaryImage = false
                       }
                   },
                   IsFeatured = true
               },
                new Vehicle
               {
                   VIN = "1Z045LL8271",
                   Make = new MakeModel
                   {
                       MakeId = 3,
                       MakeName = "Honda",
                       ModelName = "Civic"
                   },
                   Year = 2017,
                   VehicleType = VehicleType.New,
                   BodyType = BodyType.Hatchback,
                   Transmission = Transmission.SemiAutomatic,
                   ExteriorColor = Color.Blue,
                   InteriorColor = Color.Gray,
                   Mileage = 230,
                   MSRP = 21500,
                   SalePrice = 20500,
                   Description = "Brand new 2017, fun, reliable car at a great price. Don't miss out on this one!",
                   Images = new List<Image>
                   {
                       new Image
                       {
                         ImageId = 9,
                         ImageLink = "/Content/CarImages/Civic1.JPG",
                         IsPrimaryImage = true
                       },
                       new Image
                       {
                           ImageId = 10,
                           ImageLink ="/Content/CarImages/Civic2.JPG",
                           IsPrimaryImage = false
                       },
                       new Image
                       {
                           ImageId = 11,
                           ImageLink ="/Content/CarImages/Civic3.JPG",
                           IsPrimaryImage = false
                       },
                       new Image
                       {
                           ImageId = 12,
                           ImageLink = "/Content/CarImages/Civic4.JPG",
                           IsPrimaryImage = false
                       }
                   },
                   IsFeatured = false
               }
           };
        }

        public List<Vehicle> GetAllVehicles()
        {
            return _vehicles;
        }

        public Vehicle GetVehicleById(string vehicleId)
        {
            return _vehicles.Find(v => v.VIN == vehicleId);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }

        public void DeleteVehicle(string vehicleId)
        {
            Vehicle toDelete = _vehicles.Find(v => v.VIN == vehicleId);
            _vehicles.Remove(toDelete);
        }

        public void EditVehicle(Vehicle vehicle)
        {
            Vehicle toEdit = _vehicles.Find(v => v.VIN == vehicle.VIN);
            _vehicles.Remove(toEdit);
            _vehicles.Add(vehicle);
        }
    }
}
