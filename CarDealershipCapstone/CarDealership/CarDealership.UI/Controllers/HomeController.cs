﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;

namespace CarDealership.UI.Controllers
{
    public class HomeController : Controller
    {
        VehicleManager manager = new VehicleManager();
        public ActionResult Index()
        {
            var featured = manager.GetFeaturedVehicles();
            return View(featured);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}