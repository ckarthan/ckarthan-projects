﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Factories;
using CarDealership.Data.Interfaces;
using CarDealership.Models;

namespace CarDealership.BLL
{
    public class VehicleManager
    {
        private IVehicle _vehicle;

        public VehicleManager()
        {
            _vehicle = VehicleFactory.CreateVehicleRepo();
        }
        public List<Vehicle> GetFeaturedVehicles()
        {
            var allVehicles = _vehicle.GetAllVehicles();
            var onlyFeatured = allVehicles.Where(v => v.IsFeatured).ToList();
            return onlyFeatured;
        }
    }
}
